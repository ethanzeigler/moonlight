class Property:
    def __init__(
        self,
        container: str,
        flags: int,
        hash: int,
        name: str,
        offset: int,
        size: int,
        etype: str,
    ):
        self.container = container
        self.flags = flags
        self.hash = hash
        self.name = name
        self.offset = offset
        self.size = size
        self.etype = etype

    def __repr__(self):
        return (
            f"Property:\n"
            f"\tcontainer = {self.container}\n"
            f"\tname = {self.name}\n"
            f"\tflags = {self.flags} \n"
            f"\thash = {self.hash}\n"
            f"\toffset = {self.offset}\n"
            f"\tsize = {self.size}\n"
            f"\tetype = {self.etype}"
        )


class KIObject:
    def __init__(
        self,
        name: str,
        parent: str,
        hash: int,
        fields: list,
    ):
        self.name = name
        self.parent = parent
        self.hash = hash
        self.properties = {}
        for prop in fields:
            container = prop["container"]
            flags = prop["flags"]
            propHash = prop["hash"]
            propName = prop["name"]
            offset = prop["offset"]
            size = prop["size"]
            etype = prop["type"]
            self.properties[propHash] = Property(
                container=container,
                flags=flags,
                hash=propHash,
                name=propName,
                offset=offset,
                size=size,
                etype=etype,
            )

    def __repr__(self):
        return (
            f"KIObject:\n"
            f"\tname = {self.name}\n"
            f"\tparent = {self.parent}\n"
            f"\thash = {self.hash}\n"
            f"\tproperties = {self.properties}"
        )