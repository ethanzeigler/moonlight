# Why are these DML files here again?

We don't want to have to update the logic within the tests
each time the DML message protocol changes. Therefore, it makes
more sense for us to have "duplicates" and keep tests working.