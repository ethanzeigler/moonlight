import sys, click,json, os.path
from ki.kiobject import KIObject
        
def jsonLoader(infile):
    """ @param inFile: Path to Json, Path is verified valid before function is called.
        @return: Dictionary of json to KIObjects
        Function parses incoming json into a dictionary of KIObjects and loads in their properties
        """
    objectDict = {}
    with open(infile) as json_file:
        data = json.load(json_file)
        for item in data['classes']:
            name = item
            try:
                parent = data['classes'][item]['parent']
            except KeyError:
                parent = None
            objectHash = data['classes'][item]['hash']
            props = data['classes'][item]['properties']          
            objectDict[objectHash] = KIObject(name=name,parent=parent,hash=objectHash,fields=props)
    return objectDict

@click.command()
@click.option('-f','--file-path','jsonPath', required=1, help='Path to Class Dump Json')
def start(jsonPath=""):
    """Program parses passed path to dictionary  of KIObjects"""
    if os.path.exists(jsonPath):
        objectDict = jsonLoader(jsonPath)
        sys.exit(0)
    else:
        click.echo(f'File: {jsonPath} could not be found, check path!')
        sys.exit(1)    

if __name__ == "__main__":
   start()